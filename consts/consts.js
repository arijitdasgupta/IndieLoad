export const defaultStrokeWidth = 10;
export const backgroundStrokeColor = '#e3e3e3';
export const arcStrokeColor = 'blue';
export const defaultTextFontSize = '30px';
