import React from 'react';
import renderer from 'react-test-renderer';

import { CircleBackground } from '../../../components/CircleBackground.jsx';

describe('CircleBackground', () => {
    it('Should render with defaults', () => {
        const tree = renderer
            .create(<CircleBackground />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Should render with props', () => {
        const tree = renderer
            .create(<CircleBackground strokeColor={'#223535'} strokeWidth={4} />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});