import React from 'react';
import renderer from 'react-test-renderer';

import { Text } from '../../../components/Text.jsx';

describe('Text', () => {
    it('Should render with defaults', () => {
        const tree = renderer
            .create(<Text />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Should render with props', () => {
        const tree = renderer
            .create(<Text percentage={42} />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});