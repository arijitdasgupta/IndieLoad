import React from 'react';
import renderer from 'react-test-renderer';

import { SmallArc } from '../../../components/SmallArc.jsx';

describe('SmallArc', () => {
    it('Should render with defaults', () => {
        const tree = renderer
            .create(<SmallArc />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Should render with props', () => {
        const tree = renderer
            .create(<SmallArc 
                strokeWidth={10}
                strokeColor={'black'}
                percentage={42}
            />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});