import React from 'react';
import renderer from 'react-test-renderer';

import { Svg } from '../../../components/Svg.jsx';

describe('Svg', () => {
    it('Should render with children', () => {
        const tree = renderer
            .create(<Svg><p>A</p><p>B</p></Svg>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Should render with rotation period', () => {
        const tree = renderer
            .create(<Svg rotationPeriod={10} />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Should render with default', () => {
        const tree = renderer
            .create(<Svg rotationPeriod={'11'} />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});