import ReactDOM from 'react-dom';
import React from 'react';

import { IndieLoad } from './components/IndieLoad.jsx';

class IndieLoadWrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            percentage: 0,
            running: false
        }

        this.interval = null;

        this.startInterval = this.startInterval.bind(this);
        this.clearInterval = this.clearInterval.bind(this);
        this.handleStartStop = this.handleStartStop.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    startInterval() {
        this.interval = setInterval(() => {
            const state = this.state;
            state.percentage += 1;
            this.setState(state);
        }, (Math.random() + 0.2) * 1000);

        this.state.running = true;
        this.setState(this.state);
    }

    clearInterval() {
        clearInterval(this.interval);
        this.interval = null;

        this.state.running = false;
        this.setState(this.state);
    }

    componentDidMount() {
        this.startInterval();
    }

    handleStartStop() {
        if (this.interval) {
            this.clearInterval();
        } else {
            this.startInterval();
        }
    }

    handleReset() {
        this.state.percentage = 0;
        this.setState(this.state);
    }

    render() {
        const props = Object.assign({}, this.props, {
            percentage: this.state.percentage
        });

        return <div>
            <IndieLoad {...props} />
            <button style={{display:'inline'}} onClick={this.handleStartStop}>
                {this.state.running ? 'Stop' : 'Start'}
            </button>
            <button style={{display:'inline'}} onClick={this.handleReset}>Reset</button>
        </div>;
    }
}

const initializeApplication = () => {
    const mainAppDiv = document.getElementById('main-application');

    const styles = {
        width: '200px',
        height: '200px',
        margin: '5px',
        display: 'inline-block'
    }

    // To demonstrate variety of possible variation of the loader
    const variety = [
        {
            strokeColor: "orange"
        },
        {
            strokeColor: "pink",
            textFontSize: "40px",
            rotationPeriod: 7
        },
        {
            strokeColor: "yellow",
            backgroundStrokeColor: "orange"
        },
        {
            strokeColor: "yellow",
            strokeWidth: 7,
            rotationPeriod: 2
        },
        {
            strokeColor: "pink",
            backgroundStrokeColor: "white",
            strokeWidth: 10,
            rotationPeriod: 1
        }
    ];

    ReactDOM.render(
        <div>
            {variety.map((propObj, i) => <div key={i} style={styles}><IndieLoadWrapper {...propObj} /></div>)}
        </div>
        , mainAppDiv
    );
}

initializeApplication();
