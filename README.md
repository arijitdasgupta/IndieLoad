# IndieLoad

## A React spinning loading indicator

To run in dev mode,
```bash
npm install
npm run watch

# Once webpack starts watching visit http://localhost:8080/ in your browser, you should see a bunch of sample usage of the component `<IndieLoad />`
```

To build from this source,
```bash
npm run build
```

For tests,
```bash
npm run test
```

To watch tests,
```bash
npm run test -- --watch
```

### Usage

```javascript
import { IndieLoad } from 'indie-load';
```

The component `<IndieLoad />` accepts the following props,

```
backgroundStrokeColor: <CSS Color>
strokeColor: <CSS Color> 
strokeWidth: <Integer | SVG Stroke Width> {max: 10}
rotationPeriod: <Integer> {in seconds}
percentage: <Number>
textFontSize: <CSS Size>
```

It takes up `100% width` and `100% height` of it's container, so make sure it's container has a size set properly. In need of examples, check out `index.jsx` sample app has a bunch of usage.

TODO:
====
 - Use React `propTypes` for development build checks.
 - Optimize build size for production.
 - Add more props to control details of the circular drawing.

