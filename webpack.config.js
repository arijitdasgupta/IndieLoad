var path = require('path');

module.exports = {
    entry: "./index.jsx",
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: "index.js",
        publicPath: 'build'
    },

    devtool: "source-map",

    devServer: {
        inline: true,
    },

    module: {
        loaders: [
            {
                test : /\.jsx?/,
                loader : 'babel-loader'
            },
            { test: /\.scss$/, use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader",
                }]
            }
        ]
    }
};
