import React from 'react';

import { defaultTextFontSize } from '../consts/consts';

export const Text = ({percentage, textFontSize}) => {
    const foreDivStyle = {
        position: 'absolute',
        width: '100%',
        height: '100%',
        display: 'flex',
        top: '0',
        left: '0',
        alignItems: 'center',
        justifyContent: 'center'
    };    

    const textStyle = {
        margin: 0,
        padding: 0,
        textAlign: 'center',
        display: 'inline',
        fontFamily: 'helvetica',
        fontSize: textFontSize || defaultTextFontSize
    };

    return <div style={foreDivStyle}>
        <p style={textStyle}>{percentage + '%'}</p>
    </div>;    
}