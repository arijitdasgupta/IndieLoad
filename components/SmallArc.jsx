import React from 'react';
import { arcStrokeColor, defaultStrokeWidth } from '../consts/consts';

// SVG view window 0, 0, 110, 110

export const SmallArc = (props) => {
    const strokeColor = props.strokeColor || arcStrokeColor;
    const strokeWidth = props.strokeWidth || defaultStrokeWidth;

    const percent = props.percentage > 99 ? 99 : props.percentage;

    const angle = ( percent / 100 ) * ( 2 * Math.PI ); 

    const RAD = 50;

    const startY = 5;

    const endX = 55 + Math.sin(angle) * RAD;
    const endY = 55 - Math.cos(angle) * RAD;

    const largeArc = angle > Math.PI ? 1 : 0;

    return <path d={`M55 ${startY} A ${RAD} ${RAD} 0 ${largeArc} 1 ${endX} ${endY}`} fill='none' strokeLinecap = 'round' stroke={strokeColor} strokeWidth={strokeWidth} />
}