import React from 'react';

export const Svg = (props) => {
    const speed = !isNaN(props.rotationPeriod) ? props.rotationPeriod + 's' : '4s';

    const rotationStyle = {
        animationName: 'indie-loader-spin',
        animationDuration: speed,
        animationIterationCount: 'infinite',
        animationTimingFunction: 'linear'
    };

    return <svg style={rotationStyle} viewBox="0 0 110 110" width='100%' height='100%'>
        {props.children}
    </svg>;
}