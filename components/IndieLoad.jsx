import React from 'react';

import { Svg } from './Svg.jsx';
import { CircleBackground } from './CircleBackground.jsx';
import { SmallArc } from './SmallArc.jsx';
import { Text } from './Text.jsx';

import '../styles/indieLoad.scss';

export const IndieLoad = (props) => {
    const { 
        backgroundStrokeColor, 
        strokeColor, 
        strokeWidth, 
        rotationPeriod,
        percentage,
        textFontSize
    } = props;

    // Clip value...
    let percent = isNaN(percentage) ? 0 : Math.floor(percentage);
    percent = percentage < 0 ? 0 : percentage;
    percent = percent > 100 ? 100 : percentage;

    const divStyle = {
        position: 'relative'
    };

    return <div style={divStyle}>
        <Svg rotationPeriod={rotationPeriod} >
            <CircleBackground strokeColor={backgroundStrokeColor} strokeWidth={strokeWidth} />
            <SmallArc percentage={percent} strokeColor={strokeColor} strokeWidth={strokeWidth} />
        </Svg>
        <Text textFontSize={textFontSize} percentage={percent} />
    </div>;
};