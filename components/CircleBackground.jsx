import React from 'react';
import { backgroundStrokeColor, defaultStrokeWidth } from '../consts/consts';

// SVG view window 0, 0, 110, 110

export const CircleBackground = (props) => {
    const strokeColor = props.strokeColor || backgroundStrokeColor;
    const strokeWidth = props.strokeWidth || defaultStrokeWidth;

    return <circle cx="55" cy="55" r="50" fill="none" stroke={strokeColor} strokeWidth={strokeWidth} strokeLinecap='round' />
}